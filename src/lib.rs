use std::fmt;
use std::io::{self, BufRead, Write};
use std::net;
use std::str::FromStr;

pub mod commands;
pub mod responses;
mod codes {
    pub(crate) const CONNECT_OK: &str = "220";
    pub(crate) const DEFINITIONS_RETRIEVED: &str = "150";
    pub(crate) const DEFINITION_START: &str = "151";
    pub(crate) const MATCHES_RETRIEVED: &str = "152";
    pub(crate) const OK: &str = "250";
    pub(crate) const NO_MATCH: &str = "552";
}

use commands::Command;

pub struct Dict(net::TcpStream);

impl Dict {
    pub fn connect(server: &str, port: u16) -> io::Result<Self> {
        let stream = net::TcpStream::connect(&format!("{}:{}", server, port))?;
        let mut lines = io::BufReader::new(stream.try_clone()?).lines();
        match lines.next() {
            Some(line) => {
                if line?.starts_with(codes::CONNECT_OK) {
                    Ok(Self(stream))
                } else {
                    Err(io::Error::new(io::ErrorKind::ConnectionRefused, "Could not connect."))
                }
            },
            None => Err(io::Error::new(io::ErrorKind::ConnectionRefused, "Could not connect.")),
        }
    }

    pub fn define(&mut self, define: Command) -> io::Result<Option<responses::Definitions>> {
        self.0.write(define.to_string().as_bytes())?;
        let mut lines = io::BufReader::new(self.0.try_clone()?).lines();
        if let Some(Ok(line)) = lines.next() {
            if line.starts_with(codes::DEFINITIONS_RETRIEVED) {
                return Ok(Some(responses::Definitions::process(lines)));
            } else if line.starts_with(codes::NO_MATCH) {
                return Ok(None);
            } else {
                return Err(io::Error::new(io::ErrorKind::Other, line));
            }
        }

        Err(io::Error::new(io::ErrorKind::Other, "No data."))
    }

    pub fn r#match(&mut self, r#match: Command) -> io::Result<Option<responses::Matches>> {
        self.0.write(r#match.to_string().as_bytes())?;
        let mut lines = io::BufReader::new(self.0.try_clone()?).lines();
        if let Some(Ok(line)) = lines.next() {
            if line.starts_with(codes::MATCHES_RETRIEVED) {
                return Ok(Some(responses::Matches::process(lines)));
            } else if line.starts_with(codes::NO_MATCH) {
                return Ok(None);
            } else {
                return Err(io::Error::new(io::ErrorKind::Other, line));
            }
        }

        Err(io::Error::new(io::ErrorKind::Other, "No data."))
    }

    pub fn show_strategies(&mut self) -> io::Result<responses::Strategies> {
        self.0.write(Command::ShowStrategies.to_string().as_bytes())?;
        Ok(responses::Strategies(io::BufReader::new(self.0.try_clone()?).lines()))
    }

    pub fn show_databases(&mut self) -> io::Result<responses::Databases> {
        self.0.write(Command::ShowDatabases.to_string().as_bytes())?;
        Ok(responses::Databases(io::BufReader::new(self.0.try_clone()?).lines()))
    }
}

impl Drop for Dict {
    fn drop(&mut self) {
        let _ = self.0.write(Command::Quit.to_string().as_bytes());
    }
}

#[derive(Clone, PartialEq, Eq, Hash)]
pub enum Database {
    AllMatches,
    FirstMatch,
    Database(String),
}

impl FromStr for Database {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let database = match s {
            "*" => Database::AllMatches,
            "!" => Database::FirstMatch,
            db => Database::Database(String::from(db)),
        };

        Ok(database)
    }
}

impl fmt::Display for Database {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let database = match self {
            Database::AllMatches => "*",
            Database::FirstMatch => "!",
            Database::Database(db) => db,
        };
        write!(f, "{}", database)
    }
}

pub enum Strategy {
    Exact,
    Prefix,
    Strategy(String),
}

impl FromStr for Strategy {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let strategy = match s {
            "exact" => Strategy::Exact,
            "prefix" => Strategy::Prefix,
            strat => Strategy::Strategy(String::from(strat)),
        };

        Ok(strategy)
    }
}

impl fmt::Display for Strategy {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let strategy = match self {
            Strategy::Exact => "exact",
            Strategy::Prefix => "prefix",
            Strategy::Strategy(strat) => strat,
        };
        write!(f, "{}", strategy)
    }
}


#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    #[ignore]
    fn dict_connect() {
        Dict::connect("dict.org", 2628).unwrap();
    }
}
