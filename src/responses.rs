use std::fmt;
use std::io;
use std::net;

use crate::codes;

pub struct Definitions(io::Lines<io::BufReader<net::TcpStream>>);

impl Definitions {
    pub(crate) fn process(lines: io::Lines<io::BufReader<net::TcpStream>>) -> Self {
        Self(lines)
    }
}

impl Iterator for Definitions {
    type Item = Definition;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(Ok(line)) = self.0.next() {
            if ! line.starts_with(codes::DEFINITION_START) {
                return None;
            }
            let database = line.chars().skip(4).collect::<String>();
            let mut defn: Vec<String> = Vec::new();
            for line in &mut self.0 {
                if let Ok(line) = line {
                    if line == "." {
                        return Some(Definition {
                            database,
                            definition: defn.join("\n"),
                        });
                    } else if line.starts_with(codes::OK) {
                        return None;
                    } else {
                        defn.push(line);
                    }
                } else {
                    return None;
                }
            }
        }

        None
    }
}

pub struct Definition {
    pub database: String,
    pub definition: String,
}

pub struct Matches(io::Lines<io::BufReader<net::TcpStream>>);

impl Matches {
    pub(crate) fn process(lines: io::Lines<io::BufReader<net::TcpStream>>) -> Self {
        Self(lines)
    }
}

impl Iterator for Matches {
    type Item = Match;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(Ok(line)) = self.0.next() {
            if line == "." {
                return None;
            } else {
                let split: Vec<&str> = line.splitn(2, ' ').collect();
                let database = split[0].parse().unwrap();
                let word = split[1].trim_matches('"').to_string();
                return Some(Match { database, word });
            }
        }

        None
    }
}

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Match {
    pub database: crate::Database,
    pub word: String,
}

pub struct Strategies(pub(crate) io::Lines<io::BufReader<net::TcpStream>>);

impl Iterator for Strategies {
    type Item = Strategy;

    fn next(&mut self) -> Option<Self::Item> {
        for line in &mut self.0 {
            if let Ok(line) = line {
                if line.starts_with("111") {
                } else if line == "." {
                    return None;
                } else {
                    let split: Vec<&str> = line.splitn(2, ' ').collect();
                    let strategy: crate::Strategy = split[0].parse().unwrap();
                    let description = split[1].trim_matches('"').to_string();
                    return Some(Strategy { strategy, description });
                }
            } else {
                return None;
            }
        }

        None
    }
}

pub struct Strategy {
    pub strategy: crate::Strategy,
    pub description: String,
}

impl fmt::Display for Strategy {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} -- {}", self.strategy, self.description)
    }
}

pub struct Databases(pub(crate) io::Lines<io::BufReader<net::TcpStream>>);

impl Iterator for Databases {
    type Item = Database;

    fn next(&mut self) -> Option<Self::Item> {
        for line in &mut self.0 {
            if let Ok(line) = line {
                if line.starts_with("110") {
                } else if line == "." {
                    return None;
                } else {
                    let split: Vec<&str> = line.splitn(2, ' ').collect();
                    let database: crate::Database = split[0].parse().unwrap();
                    let description = split[1].trim_matches('"').to_string();
                    return Some(Database { database, description });
                }
            } else {
                return None;
            }
        }

        None
    }
}

pub struct Database {
    pub database: crate::Database,
    pub description: String,
}

impl fmt::Display for Database {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} -- {}", self.database, self.description)
    }
}
