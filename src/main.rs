use std::collections::HashSet;

use clap::{App, Arg};
use dictp::{Database, Dict, Strategy, commands::Command, responses::{Definition, Match}};

fn main() {
    let args = App::new("dict")
        .arg(Arg::with_name("server")
             .short("S")
             .long("server")
             .value_name("SERVER")
             .help("DICT server to query; default: dict.org")
             .takes_value(true)
        )
        .arg(Arg::with_name("port")
             .short("P")
             .long("port")
             .value_name("PORT")
             .help("DICT server port; default: 2628")
             .takes_value(true)
        )
        .arg(Arg::with_name("database")
             .short("d")
             .long("database")
             .value_name("DATABASE")
             .help("DICT database to filter by, or '*' for all matches or '!' for first match; default: *")
             .takes_value(true)
        )
        .arg(Arg::with_name("strategy")
             .short("s")
             .long("strategy")
             .value_name("STRATEGY")
             .help("Use MATCH strategy to get definitions based on match.")
             .takes_value(true)
        )
        .arg(Arg::with_name("word")
             .value_name("WORD")
             .index(1)
        )
        .arg(Arg::with_name("show")
             .long("show")
             .value_name("SHOW")
             .help("Show one of [strategies, databases].")
             .takes_value(true)
        )
        .get_matches();

    let server = args.value_of("server").unwrap_or("dict.org");
    let port = args
        .value_of("port")
        .unwrap_or("2628")
        .parse::<u16>()
        .expect("Invalid port number supplied.");

    let mut dict = Dict::connect(server, port).expect("Could not connect to dict server.");

    if let Some(show) = args.value_of("show") {
        match show.to_lowercase().as_str() {
            "strat" | "strategies" => {
                for strat in dict.show_strategies().expect("Could not retrieve strategies.") {
                    println!("{}", strat);
                }
            },
            "db" | "databases" => {
                for db in dict.show_databases().expect("Could not retrieve databases.") {
                    println!("{}", db);
                }
            },
            _ => panic!("Invalid entitiy for SHOW command."),
        }
        return;
    }

    let word = args.value_of("word").expect("No WORD provided to look up.");

    let database = args
        .value_of("database")
        .unwrap_or("*")
        .parse::<Database>()
        .expect("Invalid database supplied.");

    if let Some(strategy) = args.value_of("strategy") {
        let strategy = strategy.parse::<Strategy>().expect("Invalid strategy supplied.");
        let cmd = Command::Match(database, strategy, word.to_string());
        let matches: Option<Vec<Match>> = dict.r#match(cmd)
            .expect("Problem getting results.")
            .map(|matches| matches.collect());
        if let Some(matches) = matches {
            let mut count = 0;
            let mut unique_matches = HashSet::new();
            for m in matches {
                if ! unique_matches.contains(&m) {
                    unique_matches.insert(m.clone());
                    let cmd = Command::Define(m.database, m.word);
                    let defns = dict.define(cmd).expect("Problem getting results.");

                    if count > 0 {
                        println!("\n");
                    }
                    count += 1;
                    print_definitions(defns);
                }
            }
        }
    } else {
        let cmd = Command::Define(database, word.to_string());
        let defns = dict.define(cmd).expect("Problem getting results.");
        print_definitions(defns);
    }
}

fn print_definitions(defns: Option<impl Iterator<Item=Definition>>) {
    if let Some(defns) = defns {
        let mut count = 0;
        for defn in defns {
            if count > 0 {
                println!("\n");
            }
            count += 1;
            println!("\x1b[1;4m{}\x1b[0m\n{}", defn.database, defn.definition);
        }
    }
}
