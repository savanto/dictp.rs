use std::fmt;

use crate::{Database, Strategy};

pub enum Command {
    Define(Database, String),
    Match(Database, Strategy, String),
    ShowStrategies,
    ShowDatabases,
    Quit,
}

impl fmt::Display for Command {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Command::Define(database, word) => write!(f, "DEFINE {} \"{}\"\n", database, word),
            Command::Match(database, strategy, word) => write!(f, "MATCH {} {} \"{}\"\n", database, strategy, word),
            Command::ShowStrategies => write!(f, "SHOW STRATEGIES\n"),
            Command::ShowDatabases => write!(f, "SHOW DATABASES\n"),
            Command::Quit => write!(f, "QUIT\n"),
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::commands::*;

    #[test]
    fn define_all_cmd() {
        let db = Database::AllMatches;
        let cmd = Command::Define(db, "penguin".to_string());
        assert_eq!(cmd.to_string(), "DEFINE * \"penguin\"\n");
    }

    #[test]
    fn define_first_cmd() {
        let db = Database::FirstMatch;
        let cmd = Command::Define(db, "penguin".to_string());
        assert_eq!(cmd.to_string(), "DEFINE ! \"penguin\"\n");
    }

    #[test]
    fn define_database_cmd() {
        let db = Database::Database("wordnet".to_string());
        let cmd = Command::Define(db, "penguin".to_string());
        assert_eq!(cmd.to_string(), "DEFINE wordnet \"penguin\"\n");
    }
}
